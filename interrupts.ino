//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//do_Left_Encoder() Definitions
void do_Left_Encoder()
{
  // Test transition; since the interrupt will only fire on 'rising' we don't need to read pin A
  LeftEncoderBSet = digitalRead(Left_Encoder_PinB);   // read the input pin
  Left_Encoder_Ticks -= LeftEncoderBSet ? -1 : +1;
  if (digitalRead(Left_Encoder_PinB))
  {
    pulseCounter_2++;
    motor2_dir_read = true;
  }
  else
  {
    pulseCounter_2--;
    motor2_dir_read = false;
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//do_Right_Encoder() Definitions

void do_Right_Encoder()
{

  RightEncoderBSet = digitalRead(Right_Encoder_PinB);   // read the input pin
  Right_Encoder_Ticks += RightEncoderBSet ? -1 : +1;
  if (!digitalRead(Right_Encoder_PinB))
  {
    pulseCounter_1++;
    motor1_dir_read = true;
  }
  else
  {
    pulseCounter_1--;
    motor1_dir_read = false;
  }

}
