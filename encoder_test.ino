///////////////////////////////////////////////////////////////
//Encoder pins definition

// Left encoder

#define Left_Encoder_PinA 3
#define Left_Encoder_PinB 4

volatile long Left_Encoder_Ticks = 0;
volatile bool LeftEncoderBSet;

//Right Encoder

#define Right_Encoder_PinA 2
#define Right_Encoder_PinB 7


#define RPM_TEST_SAMPLE_TIME 10
long secToMin = 60000 / RPM_TEST_SAMPLE_TIME;

volatile long Right_Encoder_Ticks = 0;
volatile bool RightEncoderBSet;

volatile long pulseCounter_1 = 0;
volatile long pulseCounter_2 = 0;


volatile  double  motor1_RPM_a = 0;
volatile  double  motor2_RPM_a = 0;

unsigned long currentMillis = 0;
unsigned long previousMillis = 0;



/////////////////////////////////////////////////////////////////

volatile boolean motor1_dir_read = true ; //true -> cw false -> ccw
volatile boolean motor2_dir_read = true ;

void setup()
{
  // Left encoder
  pinMode(Left_Encoder_PinA, INPUT_PULLUP);      // sets pin A as input
  pinMode(Left_Encoder_PinB, INPUT_PULLUP);      // sets pin B as input
  //Attaching interrupt in Left_Enc_PinA.
  attachInterrupt(digitalPinToInterrupt(Left_Encoder_PinA), do_Left_Encoder, RISING);

  // Right encoder
  pinMode(Right_Encoder_PinA, INPUT_PULLUP);      // sets pin A as input
  pinMode(Right_Encoder_PinB, INPUT_PULLUP);      // sets pin B as input
  //Attaching interrupt in Right_Enc_PinA.
  attachInterrupt(digitalPinToInterrupt(Right_Encoder_PinA), do_Right_Encoder, RISING);

  Serial.begin (115200);

}




void cal_rpm ()
{
  currentMillis = millis();
  if (currentMillis - previousMillis > RPM_TEST_SAMPLE_TIME)
  {
    previousMillis = currentMillis ;

    //*******************************************************************
    motor1_RPM_a = ( pulseCounter_1 * secToMin) /1000;
    motor2_RPM_a = ( pulseCounter_2 * secToMin) / 1000;
    //********************************************************************

    pulseCounter_1 = 0;
    pulseCounter_2 = 0;
  }
}



void loop()
{
  cal_rpm ();
  Serial.print (" Right RPM\t");
  Serial.print (motor1_RPM_a);
  Serial.print ("\t");
  Serial.print (Right_Encoder_Ticks);
  Serial.print ("\t");
  Serial.print (" lift RPM\t");
  Serial.print (motor2_RPM_a);
  Serial.print ("\t");
  Serial.print (Left_Encoder_Ticks);
  Serial.print ("\n");

}
